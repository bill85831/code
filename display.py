import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State

external_stylesheets = [
    dbc.themes.BOOTSTRAP,
    'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
]

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div([
            # header
            html.H3(
                style = {
                    'textAlign': 'center',
                    'color' : '#3333CC',
                    # 將英文字母變成大寫
                    "font-variant" : "small-caps",
                    'margin-top' : '1em',
                    'margin-bottom' : '1em',
                },
                children = [
                    html.I(
                        className = "fa fa-binoculars"
                    ),
                    html.Span(
                        style = {
                            "margin-left" : "0.5em",
                        },
                    ),
                    "Fake News Analysis System",
                ]
            ),
            html.Hr(
                style= {
                    "border": 0,
                    "height": "5px",
                    "background-image": "linear-gradient(to right, hsla(240,100%,50%,1),\
                                                                   hsla(200,100%,50%,1),\
                                                                   hsla(210,100%,50%,1),\
                                                                   hsla(240,100%,50%,1),\
                                                                   hsla(210,100%,50%,1),\
                                                                   hsla(200,100%,50%,1),\
                                                                   hsla(240,100%,50%,1))"
                }
            ),
            # body
            html.Div(
                style = {
                    "margin": "0px auto",
                    "margin-top" : "1em",
                    "margin-bottom" : "1em",
                    "textAlign" : "center",
                    "font-size" : "150%"
                },
                children = [
                    html.Strong(
                        style = {
                        },
                        children = [
                            "Relationship Diagram",
                            dbc.Button(
                                id="fnarfps_btn_graph_hint_area",
                                style = {
                                    # 設定字體，兩種字體用逗點隔開
                                    "font-family" : "'Nunito', sans-serif",
                                    "display" : "inline-block",
                                    "font-weight" : "700",
                                    "font-size": "30%",
                                    # 按鈕4個角的設置
                                    "border-radius" : "40px",
                                    "text-decoration" : "none",
                                },
                                color="link",
                                # 透過classname改變button的形狀
                                children = html.I(className = "fa fa-exclamation-circle", title="Help You Understand Propagation Diagrams")
                            ),
                            dbc.Tooltip(
                                children = "Help you understand the propagation diagram.",
                                style={
                                    "font-size": "20%"
                                },
                                target = "fnarfps_btn_graph_hint_area",
                                placement = "bottom",
                            ),
                        ]
                    ),
                ]
            ),
            dbc.Collapse(
                id = "fnarfps_graph_hint_area",
                style = {
                    "margin": "0px auto",
                    "margin-top" : "2em",
                    "margin-bottom" : "2em",
                    "padding" : "2em",
                    # 邊線的寬度與顏色
                    'border': '5px #0088CC solid',
                    "box-shadow" : "5px 5px 5px #B3B3FF",
                    "-webkit-box-shadow" : "5px 5px 5px #B3B3FF",
                    "-moz-box-shadow" : "5px 5px 5px #B3B3FF",
                    "background-color" : "rgba(153, 221, 255, 0.1)",
                    "width" : "60%",
                },
                children = [
                    html.Div(
                        children = html.Strong("要點說明"),
                        style = {
                            "textAlign": "center",
                            "margin-bottom" : "1.5em",
                            "font-weight": "900",
                            "font-size": "135%"
                        }
                    ),
                    ])
])


@app.callback(
    Output("fnarfps_graph_hint_area", "is_open"),
    [Input("fnarfps_btn_graph_hint_area", "n_clicks")],
    [State("fnarfps_graph_hint_area", "is_open")],
)
def fnarfps_toggle_graph_hint_area(n_clicks, is_open):
    if not n_clicks:
        raise dash.exceptions.PreventUpdate
    return not is_open

if __name__ == '__main__':
    app.run_server(debug=True)