#!/usr/bin/env python
# coding: utf-8

# In[2]:


import dash
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_html_components as html
from dash.dependencies import Input, Output, State


# In[ ]:


app = dash.Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])

app.layout = html.Div(
    [
        dbc.Button(
            "Toggle left", color="primary", id="left", className="mr-1"
        ),
        dbc.Button(
            "Toggle right", color="primary", id="right", className="mr-1"
        ),
        dbc.Button("Toggle both", color="primary", id="both"),
        dbc.Tooltip(
                    "click and reveal both message.",
                    target="both",
                ),
        dbc.Row(
            [
                dbc.Col(
                    # 警示標記
                    dbc.Alert(
                        "This is the left card.",
                        id="left-collapse",
                        color="danger",
                        # 多設置一個關閉的按鈕
                        dismissable=True,
                        # 預設為關閉狀態
                        is_open=False
                    )
                ),
                dbc.Col(
                    #隱藏一個欄位
                    dbc.Collapse(
                        dbc.Card("This is the right card!", body=True),
                        id="right-collapse",
                    )
                ),
            ],
            className="mt-3"
        ),
    ]
)


@app.callback(
    Output("left-collapse", "is_open"),
    [Input("left", "n_clicks"), Input("both", "n_clicks")],
    [State("left-collapse", "is_open")],
)
def toggle_left(n_left, n_both, is_open):
    if n_left or n_both:
        return not is_open
    return is_open


@app.callback(
    Output("right-collapse", "is_open"),
    [Input("right", "n_clicks"), Input("both", "n_clicks")],
    [State("right-collapse", "is_open")],
)
def toggle_left(n_right, n_both, is_open):
    if n_right or n_both:
        return not is_open
    return is_open

if __name__ == '__main__':
    app.run_server(debug=True)

